module com.example.ejercicio {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.ejercicio to javafx.fxml;
    exports com.example.ejercicio;
}