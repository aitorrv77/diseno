package com.example.ejercicio;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.zip.CheckedOutputStream;

public class FormularioController   implements Initializable{

    private ObservableList<String> items = FXCollections.observableArrayList();
    private ObservableList<String> items1 = FXCollections.observableArrayList();
    @FXML
    void listarCiudades(MouseEvent event) {
        items.addAll("Alcoy","Cocentaina","Muro","Castalla","Xixona","Beneixama");

        }

    @FXML
    void listarSistemas(MouseEvent event) {
        items1.addAll("Windows","MacOS","Linux");

    }

    @FXML
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ComboBox<String> ciudades = new ComboBox<>(items);
        ComboBox<String> sistemas = new ComboBox<>(items1);
    }
}


